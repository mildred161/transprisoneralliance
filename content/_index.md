## Welcome

The Trans Prisoner Alliance was founded in 2019 by Sarah Jane Baker after spending 30 years as an out transgender person in British prisons. Our aim is to provide practical and emotional support to transgender people behind bars -- one of the most marginalised groups of people residing in British prisons.

